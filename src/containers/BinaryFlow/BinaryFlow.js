import ReactFlow, { Controls, Background } from 'reactflow';
import 'reactflow/dist/style.css';


function BinaryFlow({nodes, edges, onNodesChange, onEdgesChange}) {



    return (
      <div style={{ height:'700px', width:'100wv', margin:'auto'}}>
        <ReactFlow nodes={nodes} edges={edges} onNodesChange={onNodesChange}
      onEdgesChange={onEdgesChange}>
          <Background />
          <Controls />
        </ReactFlow>
      </div>
    );
  }
  
  export default BinaryFlow;