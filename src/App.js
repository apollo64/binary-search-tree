import './App.css';
import 'reactflow/dist/style.css';
import { useState,useCallback } from 'react';
import BinaryFlow from './containers/BinaryFlow/BinaryFlow';
import { applyEdgeChanges, applyNodeChanges } from 'reactflow';


function App() {

  

const [listOfNodes, setListOfNodes] = useState([]);
const [listOfEdges, setListOfEdges] = useState([]);
const [listOfNumbers, setListOfNumbers] = useState([]);
const [listOfPlainNumbers, setListOfPlainNumbers] = useState([]);


const onNodesChange = useCallback(
  (changes) => setListOfNodes((nds) => applyNodeChanges(changes, nds)),
  [setListOfNodes]
);
const onEdgesChange = useCallback(
  (changes) => setListOfEdges((eds) => applyEdgeChanges(changes, eds)),
  [setListOfEdges]
);


const numberRandomInt =  (min, max) => {
  while (true){
    let number = Math.floor(Math.random() * (max - min + 1) + min);
    if (listOfPlainNumbers.length === 0) {
      let copylist = [...listOfPlainNumbers];
      copylist.push(number);
       setListOfPlainNumbers(copylist);
      return number;
    }
    if (!binarySearch(listOfPlainNumbers, number)) {
      setListOfPlainNumbers([...listOfPlainNumbers, number]);
      return number
    }
  }
};

const newNumberObject = (number) => {
  let newNumber = {
    value: number,
    left: null,
    right: null,
  };
  return newNumber;
};

const binarySearch = (listOfPlainNumbers, number) => {
  if (listOfPlainNumbers.length === 0) return false;
  let start = 0;
  let end = listOfPlainNumbers.length - 1;
  let middle = Math.floor((start + end) / 2);
  while (listOfPlainNumbers[middle] !== number && start <= end) {
    if (number < listOfPlainNumbers[middle]) {
      end = middle - 1;
    } else {
      start = middle + 1;
    }
    middle = Math.floor((start + end) / 2);
  }
  if (listOfPlainNumbers[middle] === number) {
    return true;
  }
  return false;
};

const searchOfNodeByValue = (number) => {
  let node = listOfNodes.find((node) => node.data.label === number.toString());
  return node;
};

const addNumberToBinarySearchTree = async (number) => {

  let newNumber = newNumberObject(number);
  if (listOfNumbers.length === 0) {
   
    nodeMaker(null, number);
    setListOfNumbers([...listOfNumbers, newNumber]);
    return listOfNumbers;
  } else {
    let copyListOfNumbers = [...listOfNumbers];
    let current = copyListOfNumbers[0];
    while (true) {
      let parentNode = null;
      let node = null;
      if (number === current.value) return undefined;
      if (number < current.value) {
        if (current.left === null) {
          parentNode =  searchOfNodeByValue(current.value);
          node = nodeMaker(parentNode, number);
          edgeMaker(parentNode, node);
          current.left = newNumber;
          setListOfNumbers(copyListOfNumbers);
          return copyListOfNumbers;
        }
        current = current.left;
      } else {
        if (current.right === null) {
          parentNode = searchOfNodeByValue(current.value);
          node = nodeMaker(parentNode, number);
          edgeMaker(parentNode, node);
          current.right = newNumber;
          setListOfNumbers(copyListOfNumbers);
          return copyListOfNumbers;
        }
        current = current.right;
      }
    }
  }
};
const handleBts = async  () => {
  
    let number = numberRandomInt(1, 100);
    addNumberToBinarySearchTree(number);

};


const nodeMaker =  (parentNode, number) => {
  let node = {
    id: `${number}`,
    data: { label: `${number}` },
    position: { x: 200, y: 0 },
    parentNode: null,
  };
  if (listOfNodes.length!==0) {
    node.parentNode=parentNode.data.label;
    let newY = 100;
    let newX = 100;
    if(parentNode.data.label > number) {
      newX = -100;
    }else if(parentNode.data.label < number) {
      newX = 100;
    }
    node.position = { x: newX, y: newY };

} 

  setListOfNodes([...listOfNodes, node]);
  return node;
};

const edgeMaker = (node1, node2) => {
  if (listOfNodes.length === 0) return undefined;
  let edge = {
    id: `${node1.id}-${node2.id}`,
    source: node1.id,
    target: node2.id,
    animated: true,
    arrowHeadType: 'arrowclosed',
    labelStyle: { fill: '#f6ab6c', fontWeight: 700 },
    style: { stroke: '#f6ab6c' },
    type: 'smoothstep',
  };
  setListOfEdges([...listOfEdges, edge]);
  return;
};

// arrow function hadleSpaceBarPress, which will be called when the space bar is pressed
const handleSpaceBarPress = (event) => {
  if (event.keyCode === 32 )  {
      
      handleBts();
  }
};




  return (
    <div className="App" tabIndex='0' onKeyDown={(e)=>handleSpaceBarPress(e)}>
      <header className="App-header" >
        <p>
          Binary Search Tree
        </p>
   
      </header>

      <BinaryFlow nodes={listOfNodes} edges={listOfEdges} onNodesChange={onNodesChange}
      onEdgesChange={onEdgesChange}  />

    </div>
  );
}

export default App;
